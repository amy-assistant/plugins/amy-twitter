FROM python:alpine

ENV PRODUCTION=1

ENV NAME="twitter"

RUN pip install amy tweepy

WORKDIR /app

COPY src .

CMD [ "python", "main.py" ]