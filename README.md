# Amy Telegram Plugin

## Endpoints
---
> `/create/{phone}`

Create Telegram client for phone number

---
> `/login/{phone}?token={code}`

Login telegram client with code

---
> `/status/{phone}`

Get status

---
> `/start/{phone}`

Start Worker

---
> `/stop/{phone}`

Stop Worker
